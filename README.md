### A Coduxy

A Coduxy é uma empresa situada em Los Angeles e atende apenas clientes americanos. Fundada por desenvolvedores, preza pela boa qualidade do código. Oferece suporte no que for possível para melhorar o ambiente de trabalho de seus programadores.

### Os Desafios

Os  desafios serão algumas maneiras que usaremos para testar os candidatos interessados nas nossas vagas. À princípio teremos desafios de diversas categorias, cada um com o foco para uma determinada vaga. Por favor, verifique as respectivas pastas acima para acessar seus desafios.

