# Desafio - Full-Stack Developer
Este desafio tem como objetivo te avaliar como pessoa desenvolvedora Full-stack: JavaScript, HTML, CSS, Wordpress e lógica de programação.

## O Desafio

Queremos montar uma versão da homepage da Kallyas apenas para devs, e queremos que essa versão seja integrada, ao final, com o Wordpress. Pode ser usado qualquer framework ou ferramenta que esteja disponível para você atingir esse objetivo, desde que entregue o resultado final da maneira que esperamos.

Todas as informações de design estão disponibilizadas na pasta `designs` dentro deste repositório.

## Critérios de avaliação

- Organização do código;
- Responsividade;
- Reaproveitamento de código;
- SEO;


## Extras

- Elementor: Desenvolver o tema em Wordpress de forma que cada componente seja reconhecido e editável pelo Elementor, de mandeira que seja possível criar uma página e colocar os componentes apenas arrastando.
- Docker: Adicionar o projeto para rodar inteiramente dentro de um docker, apenas usando uma simples linha de comando como `docker-composer up`.


## Como submeter seu projeto
- Enviar todo o seu projeto através de algum Bitbucket ou Github. Não esqueça de enviar a pasta do `wp-content` do seu Wordpress!
- Envie todos os arquivos para diegol@coduxy.com, com cópia para rafaelc@coduxy.com.
- Aguarde algum contribuidor realizar o code review, e te daremos um feedback o quanto antes. :)
  